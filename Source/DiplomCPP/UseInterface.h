// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UseInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI, BlueprintType)
class UUseInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */

class DIPLOMCPP_API IUseInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Use Object Interface")
	void UseItem();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Use Object Interface")
	void UpdateMaterial();

	
	
};
