// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PlayerStatsData.h"
#include "Bag.h"
#include "InventoryItemData.h"
#include "PlayerData.generated.h"

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class DIPLOMCPP_API UPlayerData : public UObject
{
	GENERATED_BODY()
	
public:

	UPlayerData();
	
	//Inventory Class "ItemsBag"
	UPROPERTY(BlueprintReadWrite, Category = "PlayerData")
	UBag* Inventory;

	// Chest Bag
	UPROPERTY(BlueprintReadWrite, Category = "PlayerData")
	UBag* Chest;

	// Stats
	UPROPERTY(BlueprintReadWrite, Category = "PlayerData")
	UPlayerStatsData* MyCharStats;

	// Equip items
	UPROPERTY(BlueprintReadWrite, Category = "PlayerData")
	UInventoryItemData* HeadItemEquip;
	UPROPERTY(BlueprintReadWrite, Category = "PlayerData")
	UInventoryItemData* TorsoItemEquip;
	UPROPERTY(BlueprintReadWrite, Category = "PlayerData")
	UInventoryItemData* LeftArmItemEquip;
	UPROPERTY(BlueprintReadWrite, Category = "PlayerData")
	UInventoryItemData* RaightArmItemEquip;

};
