// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGGameInstance.h"


URPGGameInstance::URPGGameInstance(const FObjectInitializer& ObjectInitializer)
{
	//URPGGameInstance::Super(ObjectInitializer);

	MyCharStatsData = CreateDefaultSubobject<UPlayerData>("MyCharStats");

	// Default Characteristics
	MyCharStatsData->MyCharStats->Level = 0;
	MyCharStatsData->MyCharStats->MyStrength = 5;
	MyCharStatsData->MyCharStats->Agility = 5;
	MyCharStatsData->MyCharStats->Perception = 5;

	// Default Stats
	MyCharStatsData->MyCharStats->SpeedStaminaRestore = MyCharStatsData->MyCharStats->Agility;
	MyCharStatsData->MyCharStats->SpeedHealthRestore = MyCharStatsData->MyCharStats->MyStrength;

	MyCharStatsData->MyCharStats->HealthMax = URPGGameInstance::GetMaxHealth();
	MyCharStatsData->MyCharStats->StaminaMax = URPGGameInstance::GetMaxStamina();
	MyCharStatsData->MyCharStats->Health = MyCharStatsData->MyCharStats->HealthMax;
	MyCharStatsData->MyCharStats->Stamina = MyCharStatsData->MyCharStats->StaminaMax;
	MyCharStatsData->MyCharStats->NextLevelExp = URPGGameInstance::GetNexLevelExp();
	MyCharStatsData->MyCharStats->Damage = URPGGameInstance::GetDamage();
}

float URPGGameInstance::GetMaxHealth()
{
	return MyCharStatsData->MyCharStats->MyStrength * 20.f;
}

float URPGGameInstance::GetMaxStamina()
{
	return MyCharStatsData->MyCharStats->Agility * 20.f;
}

float URPGGameInstance::GetNexLevelExp()
{
	if (MyCharStatsData->MyCharStats->Level == 0)
	{
		return 50.f;
	}
	else
	{
		return MyCharStatsData->MyCharStats->Level * 100.f;
	}
}

float URPGGameInstance::GetDamage()
{
	return MyCharStatsData->MyCharStats->MyStrength * 5;
}
