// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PlayerStatsData.h"
#include "PlayerData.h"
#include "RPGGameInstance.h"
#include "StatsComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DIPLOMCPP_API UStatsComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UStatsComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;


public:


	UPROPERTY()
	UPlayerStatsData* MyCharStats;


	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//Get player Stats
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PlayerStats")
	float GetHealth();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PlayerStats")
	float GetStamina();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PlayerStats")
	float GetExperience();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PlayerStats")
	int32 GetPerception();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PlayerStats")
	int32 GetAgility();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PlayerStats")
	int32 GetStrength();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PlayerStats")
	int32 GetLevel();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PlayerStats")
	float GetHealthMax();
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PlayerStats")
	float GetStaminaMax();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PlayerStats")
	float GetNextLevelExp();
		
	//Set player Stats
	UFUNCTION(BlueprintCallable, Category = "PlayerStats")
	void SetStamina(float Value);

	// Initialize Player Stats 
	UFUNCTION(BlueprintCallable, Category = "PlayerStats")
	void InitStats(UPlayerStatsData* NewStats);


	//Add Experience 
	UFUNCTION(BlueprintCallable, Category = "UpdatePlayerStats")
	void AddExperience(float Value);

	UFUNCTION(BlueprintCallable, Category = "UpdatePlayerStats")
	void AddStamina(float Value);

	UFUNCTION(BlueprintCallable, Category = "UpdatePlayerStats")
	void AddStrength(float Value);

	UFUNCTION(BlueprintCallable, Category = "UpdatePlayerStats")
	void AddAgility(float Value);

	UFUNCTION(BlueprintCallable, Category = "UpdatePlayerStats")
	void AddHealth(float Value);

protected:
	float GetMaxHealth();
	float GetMaxStamina();
	float GetNexLevelExp();
	URPGGameInstance* MyGameInstance;

};
