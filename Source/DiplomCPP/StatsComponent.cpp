// Fill out your copyright notice in the Description page of Project Settings.

#include "StatsComponent.h"


// Sets default values for this component's properties
UStatsComponent::UStatsComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	
	//MyCharStats =CreateDefaultSubobject<UPlayerStatsData>(TEXT("PlayerStats"));

	//// Default Characteristics
	//MyCharStats->Level = 0;
	//MyCharStats->MyStrength = 5;
	//MyCharStats->Agility = 5;
	//MyCharStats->Perception = 5;
	//
	//// Default Stats
	//MyCharStats->SpeedStaminaRestore = MyCharStats->Agility;
	//MyCharStats->SpeedHealthRestore = MyCharStats->MyStrength;

	//MyCharStats->HealthMax = UStatsComponent::GetMaxHealth();
	//MyCharStats->StaminaMax = UStatsComponent::GetMaxStamina();
	//MyCharStats->Health = MyCharStats->HealthMax;
	//MyCharStats->Stamina = MyCharStats->StaminaMax;
	//MyCharStats->NextLevelExp = UStatsComponent::GetNexLevelExp();

}


//  --------------- Called when the game starts ---------------
void UStatsComponent::BeginPlay()
{
	Super::BeginPlay();
	UGameInstance* CurrentInstance = GetWorld()->GetGameInstance();
	MyGameInstance = (URPGGameInstance*)CurrentInstance;

}


// --------------- Called every frame ---------------
void UStatsComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Restore Stamina 
	if (MyGameInstance->MyCharStatsData->MyCharStats->Stamina < MyGameInstance->MyCharStatsData->MyCharStats->StaminaMax)
	{
		MyGameInstance->MyCharStatsData->MyCharStats->Stamina = MyGameInstance->MyCharStatsData->MyCharStats->Stamina + MyGameInstance->MyCharStatsData->MyCharStats->SpeedStaminaRestore * DeltaTime;
	}

	// Restore Health
	if (MyGameInstance->MyCharStatsData->MyCharStats->Health < MyGameInstance->MyCharStatsData->MyCharStats->HealthMax)
	{
		MyGameInstance->MyCharStatsData->MyCharStats->Health = MyGameInstance->MyCharStatsData->MyCharStats->Health + MyGameInstance->MyCharStatsData->MyCharStats->SpeedHealthRestore * DeltaTime;
	}
	
}


// --------------- Get player stats ---------------
float UStatsComponent::GetHealth()
{
	return MyGameInstance->MyCharStatsData->MyCharStats->Health;
}

float UStatsComponent::GetStamina()
{
	return MyGameInstance->MyCharStatsData->MyCharStats->Stamina;
}

float UStatsComponent::GetExperience()
{
	return MyGameInstance->MyCharStatsData->MyCharStats->Experience;
}

int32 UStatsComponent::GetPerception()
{
	return MyGameInstance->MyCharStatsData->MyCharStats->Perception;
}

int32 UStatsComponent::GetAgility()
{
	return MyGameInstance->MyCharStatsData->MyCharStats->Agility;
}

int32 UStatsComponent::GetStrength()
{
	return MyGameInstance->MyCharStatsData->MyCharStats->MyStrength;
}

int32 UStatsComponent::GetLevel()
{
	return MyGameInstance->MyCharStatsData->MyCharStats->Level;
}

float UStatsComponent::GetHealthMax()
{
	return MyGameInstance->MyCharStatsData->MyCharStats->HealthMax;
}

float UStatsComponent::GetStaminaMax()
{
	return MyGameInstance->MyCharStatsData->MyCharStats->StaminaMax;
}

float UStatsComponent::GetNextLevelExp()
{
	return MyGameInstance->MyCharStatsData->MyCharStats->NextLevelExp;
}


// --------------- Set player stats ---------------
void UStatsComponent::SetStamina(float Value)
{
	MyGameInstance->MyCharStatsData->MyCharStats->Stamina = Value;
}



// --------------- Update Player Stats and Characteristics ---------------
void UStatsComponent::InitStats(UPlayerStatsData* NewStats)
{
	// --------------- Set Characteristics ---------------
	MyGameInstance->MyCharStatsData->MyCharStats->Experience = NewStats->Experience;
	MyGameInstance->MyCharStatsData->MyCharStats->Level = NewStats->Level;
	MyGameInstance->MyCharStatsData->MyCharStats->Perception = NewStats->Perception;
	MyGameInstance->MyCharStatsData->MyCharStats->Agility = NewStats->Agility;
	MyGameInstance->MyCharStatsData->MyCharStats->MyStrength = NewStats->MyStrength;
	
	// --------------- Update Hide Settings and Stats v
	MyGameInstance->MyCharStatsData->MyCharStats->HealthMax = UStatsComponent::GetMaxHealth();
	MyGameInstance->MyCharStatsData->MyCharStats->StaminaMax = UStatsComponent::GetMaxStamina();
	MyGameInstance->MyCharStatsData->MyCharStats->SpeedHealthRestore = MyCharStats->Agility;
	MyGameInstance->MyCharStatsData->MyCharStats->SpeedStaminaRestore = MyCharStats->MyStrength;
	MyGameInstance->MyCharStatsData->MyCharStats->NextLevelExp = UStatsComponent::GetNexLevelExp();
}

// Add Experience if Experience full add new level and update next level experience
void UStatsComponent::AddExperience(float Value)
{
	float TmpExp = MyCharStats->Experience + Value;

	if (TmpExp >MyGameInstance->MyCharStatsData->MyCharStats->NextLevelExp)
	{
		MyGameInstance->MyCharStatsData->MyCharStats->Level++;
		MyGameInstance->MyCharStatsData->MyCharStats->Experience = TmpExp - MyCharStats->NextLevelExp;
		MyGameInstance->MyCharStatsData->MyCharStats->NextLevelExp = UStatsComponent::GetNexLevelExp();
	}
	else
	{
		MyGameInstance->MyCharStatsData->MyCharStats->Experience = TmpExp;
	}
}

// Add Stamina 
void UStatsComponent::AddStamina(float Value)
{

	float TmpStamina = MyGameInstance->MyCharStatsData->MyCharStats->Stamina + Value;
	if (TmpStamina > MyGameInstance->MyCharStatsData->MyCharStats->StaminaMax)
	{
		MyGameInstance->MyCharStatsData->MyCharStats->Stamina = MyGameInstance->MyCharStatsData->MyCharStats->StaminaMax;
	}
		else
	{
		MyGameInstance->MyCharStatsData->MyCharStats->Stamina = TmpStamina;
	}

}

// Add Health
void UStatsComponent::AddHealth(float Value)
{
	if (MyGameInstance->MyCharStatsData->MyCharStats->Health < MyGameInstance->MyCharStatsData->MyCharStats->HealthMax)
	{
		float TmpHealth = MyGameInstance->MyCharStatsData->MyCharStats->Health + Value;
		if (TmpHealth > MyGameInstance->MyCharStatsData->MyCharStats->HealthMax)
		{
			MyGameInstance->MyCharStatsData->MyCharStats->Health = MyGameInstance->MyCharStatsData->MyCharStats->HealthMax;
		}
		else
		{
			MyGameInstance->MyCharStatsData->MyCharStats->Health = TmpHealth;
		}
	}
}

// Add Strength
void UStatsComponent::AddStrength(float Value)
{
	MyGameInstance->MyCharStatsData->MyCharStats->MyStrength += Value;
}

// Add Agility
void UStatsComponent::AddAgility(float Value)
{
	MyGameInstance->MyCharStatsData->MyCharStats->Agility += Value;
}



// Calculate maximum health, stamina and next level
float UStatsComponent::GetMaxHealth()
{
	return MyGameInstance->MyCharStatsData->MyCharStats->MyStrength * 20.f;
}

float UStatsComponent::GetMaxStamina()
{
	return MyGameInstance->MyCharStatsData->MyCharStats->Agility * 20.f;
}

float UStatsComponent::GetNexLevelExp()
{
	if (MyGameInstance->MyCharStatsData->MyCharStats->Level == 0)
	{
		return 50.f;
	}
	else
	{
		return MyGameInstance->MyCharStatsData->MyCharStats->Level * 100.f;
	}	
}

