// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "RPGGameHUD.generated.h"

/**
 * 
 */
UCLASS()
class DIPLOMCPP_API ARPGGameHUD : public AHUD
{
	GENERATED_BODY()
	
public:
	
	UFUNCTION(BlueprintImplementableEvent, Category = "RPGMainHUD")
	void SetInventoryWidgetVisible();
	UFUNCTION(BlueprintImplementableEvent, Category = "RPGMainHUD")
	void SetStatsWidgetVisible();
	UFUNCTION(BlueprintImplementableEvent, Category = "RPGMainHUD")
	void SetComboWidgetVisible();
	UFUNCTION(BlueprintImplementableEvent, Category = "RPGMainHUD")
	void SetMainMenuWidgetVisible();
	
	//play combo from current slot
	UFUNCTION(BlueprintImplementableEvent, Category = "RPGMainHUD")
	void PlayComboSlotOne();
	UFUNCTION(BlueprintImplementableEvent, Category = "RPGMainHUD")
	void PlayComboSloTwo();
	UFUNCTION(BlueprintImplementableEvent, Category = "RPGMainHUD") // three
	void PlayComboSlotThree();
	UFUNCTION(BlueprintImplementableEvent, Category = "RPGMainHUD")
	void PlayComboSlotFore();
	UFUNCTION(BlueprintImplementableEvent, Category = "RPGMainHUD")
	void PlayComboSlotFive();
};
