// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerData.h"

UPlayerData::UPlayerData()
{
	Inventory = CreateDefaultSubobject<UBag>(TEXT("Inventory"));
	Chest = CreateDefaultSubobject<UBag>(TEXT("Chest"));
	MyCharStats = CreateDefaultSubobject<UPlayerStatsData>(TEXT("MyCharStats"));
	HeadItemEquip = CreateDefaultSubobject<UInventoryItemData>(TEXT("HeadItemEquip"));
	TorsoItemEquip = CreateDefaultSubobject<UInventoryItemData>(TEXT("TorsoItemEquip"));
	LeftArmItemEquip = CreateDefaultSubobject<UInventoryItemData>(TEXT("LeftArmItemEquip"));
	RaightArmItemEquip = CreateDefaultSubobject<UInventoryItemData>(TEXT("RaightArmItemEquip"));
}


