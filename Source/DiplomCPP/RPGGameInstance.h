// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "PlayerData.h"
#include "RPGGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class DIPLOMCPP_API URPGGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	URPGGameInstance(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(BlueprintReadWrite, Category = "PlayerData")
	UPlayerData* MyCharStatsData;
	
protected:
	float GetMaxHealth();
	float GetMaxStamina();
	float GetNexLevelExp();
	float GetDamage();
	
};
