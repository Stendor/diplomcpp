// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UseInterface.h"
#include "WeaponItem.generated.h"

UCLASS()
class DIPLOMCPP_API AWeaponItem : public AActor , public IUseInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponItem();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSetup")
	USkeletalMeshComponent* WeaponMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSetup")
	float WeaponDemage;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Use Object Interface")
	void UseItem();
	virtual void UseItem_Implementation() override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Use Object Interface")
	void UpdateMaterial();
	virtual void UpdateMaterial_Implementation() override;
	
};
