// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "StatsComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Camera/CameraComponent.h"
#include "RPGGameHUD.h"
#include "WeaponSkeletalMeshComponent.h"
//#include "RPGGameInstance.h"
#include "PlayerChar.generated.h"

UCLASS()
class DIPLOMCPP_API APlayerChar : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerChar();

	// SetUp Character
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USpringArmComponent* SpringArm;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UCameraComponent* CameraFlow;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UWeaponSkeletalMeshComponent* MyWeapon;

	UPROPERTY(BlueprintReadWrite, Category = "PlayerCharControls")
	bool IsPlayerAttack;
	
	UPROPERTY(BlueprintReadWrite, Category = "PlayerCharControls")
	bool IsPlayerEqupWeapon;

	UPROPERTY(BlueprintReadWrite, Category = "PlayerCharControls")
	bool IsPlayerBlock;

	UPROPERTY(BlueprintReadWrite, Category = "PlayerCharControls")
	bool IsCrouching;

	UPROPERTY(BlueprintReadWrite, Category = "PlayerCharControls")
	bool IsSprinting;

	UPROPERTY(BlueprintReadWrite, Category = "PlayerCharControls")
	bool IsPlayingAnim;

	UFUNCTION(BlueprintCallable, Category = "PlayerCharFunctions")
	void SetInputEnable(bool Value);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
		
	// Add Player StatsComponent
	UPROPERTY()
	UStatsComponent* CharStats;
	
	// Game instance reference
	//UPROPERTY()
	//URPGGameInstance* MyGameInstance;

	//Game HUD Reference
	UPROPERTY()
	ARPGGameHUD* MyGameHUD;


	// axis map functions
	void MoveForvard(float AxisValue);
	void MoveRight(float AxisValue);

	// hot keys
	void UseSlotOne();
	void UseSlotTwo();
	void UseSlotThree();
	void UseSlotFore();
	void UseSlotFive();
	void UseHealthBotle();
	void UseStaminaBotle();
	void WeaponEquip();


	// add main menu to viewport
	void SpawnMainMenu();
	void SpawnStatsMenu();
	void SpawnComboMenu();
	void SpawnInventoryMenu();

	// key map function
	void Sprinting();
	void StopSprinting();
	void Crouching();
	void StopCrouching();
	void Attak();
	void StopAttak();
	void Block();
	void StopBlock();


	


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable, Category = "CharStats")
	void AddStamina(float Value);


};
