// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerChar.h"
#include "Classes/Kismet/GameplayStatics.h"


// Sets default values
APlayerChar::APlayerChar()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.f);
	
	this->bUseControllerRotationPitch = false;
	this->bUseControllerRotationRoll = false;
	this->bUseControllerRotationYaw = false;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator (0.f, 540.f, 0.f);
	GetCharacterMovement()->JumpZVelocity = 550.f;
	GetCharacterMovement()->AirControl = 0.2f;
	GetCharacterMovement()->GetNavAgentPropertiesRef().bCanCrouch = true;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	SpringArm->SetupAttachment(RootComponent);
	SpringArm->TargetArmLength = 400.f;
	SpringArm->bUsePawnControlRotation = true;

	CameraFlow = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraFlow->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
	CameraFlow->bUsePawnControlRotation = false;

	CharStats = CreateDefaultSubobject<UStatsComponent>(TEXT("CharStats"));

	MyWeapon = CreateDefaultSubobject<UWeaponSkeletalMeshComponent>(TEXT("CharWeapon"));


	IsPlayerAttack = false;
	IsPlayerBlock = false;
	IsCrouching = false;
	IsSprinting = false;
	IsPlayerEqupWeapon = false;
	IsPlayingAnim = false;
}

void APlayerChar::SetInputEnable(bool Value)
{
	if (Value)
	{
		//APawn::EnableInput(UGameplayStatics::GetPlayerController(this, 0));
		SpringArm->bUsePawnControlRotation = true;
		GetCharacterMovement()->SetActive(true);
	}
	else
	{
		//APawn::DisableInput(UGameplayStatics::GetPlayerController(this, 0));
		SpringArm->bUsePawnControlRotation = false;
		GetCharacterMovement()->SetActive(false);
	}
}

// Called when the game starts or when spawned
void APlayerChar::BeginPlay()
{
	Super::BeginPlay();

	MyGameHUD = Cast<ARPGGameHUD>(UGameplayStatics::GetPlayerController(this, 0)->GetHUD());

	MyWeapon->SetVisibility(false);
	MyWeapon->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	
}



// Called every frame
void APlayerChar::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (IsSprinting)
	{
		if (CharStats->GetStamina() > 0)
		{
			APlayerChar::AddStamina(DeltaTime * -20.f);
		}
		else
		{
			CharStats->SetStamina(0.f);
			GetCharacterMovement()->MaxWalkSpeed = 400;
		}
	}
}

// Called to bind functionality to input
void APlayerChar::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//Bind Control Axis 
	PlayerInputComponent->BindAxis("MoveForvard", this, &APlayerChar::MoveForvard);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerChar::MoveRight);
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);

	//Bind Controls Actions
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("Attak", IE_Pressed, this, &APlayerChar::Attak);
	PlayerInputComponent->BindAction("Attak", IE_Released, this, &APlayerChar::StopAttak);

	PlayerInputComponent->BindAction("Block", IE_Pressed, this, &APlayerChar::Block);
	PlayerInputComponent->BindAction("Block", IE_Released, this, &APlayerChar::StopBlock);
	
	PlayerInputComponent->BindAction("Sprinting", IE_Pressed, this, &APlayerChar::Sprinting);
	PlayerInputComponent->BindAction("Sprinting", IE_Released, this, &APlayerChar::StopSprinting);

	PlayerInputComponent->BindAction("Crouching", IE_Pressed, this, &APlayerChar::Crouching);
	PlayerInputComponent->BindAction("Crouching", IE_Released, this, &APlayerChar::StopCrouching);
	
	PlayerInputComponent->BindAction("WeaponEquip", IE_Pressed, this, &APlayerChar::WeaponEquip);
	

	
	//Slots Actions
	PlayerInputComponent->BindAction("UseSlot01", IE_Pressed, this, &APlayerChar::UseSlotOne);


	PlayerInputComponent->BindAction("UseSlot02", IE_Pressed, this, &APlayerChar::UseSlotTwo);


	PlayerInputComponent->BindAction("UseSlot03", IE_Pressed, this, &APlayerChar::UseSlotThree);


	PlayerInputComponent->BindAction("UseSlot04", IE_Pressed, this, &APlayerChar::UseSlotFore);


	PlayerInputComponent->BindAction("UseSlot05", IE_Pressed, this, &APlayerChar::UseSlotFive);


	PlayerInputComponent->BindAction("UseHealthBotle", IE_Pressed, this, &APlayerChar::UseHealthBotle);


	PlayerInputComponent->BindAction("UseStaminaBotle", IE_Pressed, this, &APlayerChar::UseStaminaBotle);


	PlayerInputComponent->BindAction("MainMenu", IE_Pressed, this, &APlayerChar::SpawnMainMenu);
	PlayerInputComponent->BindAction("Inventory", IE_Pressed, this, &APlayerChar::SpawnInventoryMenu);
	PlayerInputComponent->BindAction("Stats", IE_Pressed, this, &APlayerChar::SpawnStatsMenu);
	PlayerInputComponent->BindAction("Combos", IE_Pressed, this, &APlayerChar::SpawnComboMenu);

}


void APlayerChar::MoveForvard(float AxisValue)
{
	if (Controller != nullptr)
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, AxisValue);
	}
}

void APlayerChar::MoveRight(float AxisValue)
{
	if (Controller != nullptr)
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(Direction, AxisValue);
	}
}

void APlayerChar::UseSlotOne()
{
	MyGameHUD->PlayComboSlotOne();
}


void APlayerChar::UseSlotTwo()
{
	MyGameHUD->PlayComboSloTwo();
}


void APlayerChar::UseSlotThree()
{
	MyGameHUD->PlayComboSlotThree();
}


void APlayerChar::UseSlotFore()
{
	MyGameHUD->PlayComboSlotFore();
}


void APlayerChar::UseSlotFive()
{
	MyGameHUD->PlayComboSlotFive();
}


void APlayerChar::UseHealthBotle()
{
}



void APlayerChar::UseStaminaBotle()
{
}



void APlayerChar::WeaponEquip()
{
	if (!IsPlayingAnim)
	{
		if (IsPlayerEqupWeapon)
		{
			IsPlayerEqupWeapon = false;
			GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Blue, "UnEquip Weapon", true);

			MyWeapon->SetVisibility(false);
			MyWeapon->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		}
		else
		{
			IsPlayerEqupWeapon = true;
			GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Blue, "Equip Weapon", true);

			MyWeapon->SetVisibility(true);
			MyWeapon->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		}
	}
}

void APlayerChar::SpawnMainMenu()
{
	if (MyGameHUD)
	{
		MyGameHUD->SetMainMenuWidgetVisible();
	}
}

void APlayerChar::SpawnStatsMenu()
{
	if (MyGameHUD)
	{
		MyGameHUD->SetStatsWidgetVisible();
	}
}

void APlayerChar::SpawnComboMenu()
{
	if (MyGameHUD)
	{
		MyGameHUD->SetComboWidgetVisible();
	}
}

void APlayerChar::SpawnInventoryMenu()
{
	if (MyGameHUD)
	{
		MyGameHUD->SetInventoryWidgetVisible();
	}
}

void APlayerChar::Sprinting()
{
	GetCharacterMovement()->MaxWalkSpeed = 600;
	IsSprinting = true;
}

void APlayerChar::StopSprinting()
{
	if (IsSprinting)
	{
		GetCharacterMovement()->MaxWalkSpeed = 400;
		IsSprinting = false;
	}

}

void APlayerChar::Crouching()
{
	if (CanCrouch())
	{
		Crouch();
		IsCrouching = true;
	}
}

void APlayerChar::StopCrouching()
{
	if (IsCrouching)
	{
		UnCrouch();
		IsCrouching = false;
	}
}

void APlayerChar::Attak()
{
	if (!IsPlayerBlock)
	{
		IsPlayerAttack = true;
	}
}

void APlayerChar::StopAttak()
{
	if (IsPlayerAttack)
	{
		IsPlayerAttack = false;
	}
}

void APlayerChar::Block()
{
	if (!IsPlayerAttack && !ACharacter::IsJumping())
	{
		IsPlayerBlock = true;
	}
}

void APlayerChar::StopBlock()
{
	if (IsPlayerBlock && !ACharacter::IsJumping())
	{
		IsPlayerBlock = false;
	}
}

void APlayerChar::AddStamina(float Value)
{
	CharStats->AddStamina(Value);
}
