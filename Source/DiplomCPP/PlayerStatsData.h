// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PlayerStatsData.generated.h"

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class DIPLOMCPP_API UPlayerStatsData : public UObject
{
	GENERATED_BODY()
	
	
public:
	//Player Stats
	UPROPERTY(BlueprintReadOnly, Category = "PlayerStatsData")
	float Experience;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerStatsData")
	float Health;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerStatsData")
	float Stamina;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerStatsData")
	float BlockChance;
	
	// changeable vars
	UPROPERTY(BlueprintReadOnly, Category = "PlayerStatsData")
	int32 MyStrength;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerStatsData")
	int32 Perception;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerStatsData")
	int32 Agility;

	//Player Characteristics
	UPROPERTY(BlueprintReadOnly, Category = "PlayerStatsData")
	int32 Level;

	// help vars
	UPROPERTY(BlueprintReadOnly, Category = "PlayerStatsData")
	float SpeedStaminaRestore;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerStatsData")
	float SpeedHealthRestore;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerStatsData")
	float StaminaMax;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerStatsData")
	float HealthMax;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerStatsData")
	float NextLevelExp;
	
	UPROPERTY(BlueprintReadOnly, Category = "PlayerStatsData")
	float Damage;
	
};
