// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SkeletalMeshComponent.h"
#include "WeaponSkeletalMeshComponent.generated.h"

/**
 * 
 */
UCLASS()
class DIPLOMCPP_API UWeaponSkeletalMeshComponent : public USkeletalMeshComponent
{
	GENERATED_BODY()
	
public:	
	UWeaponSkeletalMeshComponent();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSetup")
	float WeaponDemage;
	
};
